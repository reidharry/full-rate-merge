function output_data = py_strain_fullrate(strain_matfile, rtd_matfile, standard_calc_matfile)
%%
%Created by: Harry Reid
%Build Date: 5/16/19

%Rev 5/20: Add commented section that plots all channels against standard_calcs, if
%desired. This also verifies function performed as desired.

%Directions for use:
% Input arguments to function must be ('fullrate_strain_filename.mat', 'lowrate_rtd_filename.mat', 'standard_calc_filename.mat') in that order

%Assumes inputs are .mat files that have already been merged using FTQL
%tool, and that each file contains one channel group (ex. fullrate data group: PY_STRAIN & lowrate data group: PY_RTD)
%Example Function Call: py_strain_fullrate('PY_SG.mat', 'PY_RTD.mat', 'P3-04_Standard_Calcs.mat');


%%
%Interpolate lowrate data based on fullrate utc_time%
w = waitbar(0,'Loading matfiles. Loading full-rate data may take a while.');

strain_master = load(strain_matfile); waitbar(0.5,w)
rtd_master = load(rtd_matfile); waitbar(0.75,w)
standard_calc_master = load(standard_calc_matfile); waitbar(1,w)

close(w);

first_fields = [fieldnames(strain_master) fieldnames(rtd_master) fieldnames(standard_calc_master)];
strain_fullrate = strain_master.(first_fields{1});
rtd_lowrate = rtd_master.(first_fields{2});
standard_calc = standard_calc_master.(first_fields{3});

groups = fieldnames(strain_fullrate);
if(strcmp(groups{1},'properties')==0)
    strain_channel_group = groups{1};
else
    strain_channel_group = groups{2};
end
strain_channels = fieldnames(strain_fullrate.(strain_channel_group));

fullrate_utc = strain_fullrate.(strain_channel_group).(strain_channels{1}).utc_time;

groups = fieldnames(rtd_lowrate);
if(strcmp(groups{1},'properties')==0)
    rtd_channel_group = groups{1};
else
    rtd_channel_group = groups{2};
end

rtd_channels = fieldnames(rtd_lowrate.(rtd_channel_group));
rtd_fullrate = struct();
for ii = 1:numel(rtd_channels)
    lowrate_utc = rtd_lowrate.(rtd_channel_group).(rtd_channels{ii}).utc_time; %this line could probably be outside the loop. confirm
    lowrate_value = rtd_lowrate.(rtd_channel_group).(rtd_channels{ii}).value;
    rtd_fullrate.(rtd_channels{ii}).value(:,1) = interp1(lowrate_utc,lowrate_value,fullrate_utc,'linear','extrap');
end
%%
%%THERMAL COMPENSATION & TARE CALCULATIONS%%
w = waitbar(0,'Performing Thermal Compensation & Tare Calculations. Please Wait...');
PY_STRAIN = struct('PY_STRAIN_TC',[]);
for ii = 1:numel(strain_channels)
    PY_STRAIN.PY_STRAIN_TC.(strain_channels{ii}) = [];
end

count = 1; waitbar(count/(numel(strain_channels)-5),w); 
group = 'PY_STRAIN_TC'; ch_list = string([]);
%%
%CALCGROUP 1%
tc_eqn = @(u, T) u-(-1.67+0.437.*T-0.00401.*T.^2+0.00000385.*T.^3-0.000000238*T.^4+0.0000000011*T.^5)/1e6;

s_ch = 'PYSGAZP'; ch_list(count) = string(s_ch); count = count+1;
%find info about tare from standard_calc channel%
[~, stand_calc_tare_ind] = maxk(diff(standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(1:end)),3);
stand_calc_tare_ind = min(stand_calc_tare_ind) - 9; %Use index 3 seconds before tare due to slight variation in time of tare on channels.
utc_target = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).utc_time(stand_calc_tare_ind); 
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTAH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGSPZ01'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTLH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGSPZ02'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTLH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGTSL'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTTSL.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGTSR'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTTSL.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index
%%
%CALCGROUP 2%
tc_eqn = @(u, T) u-(-13.6+0.569.*T-0.00619.*T.^2+0.00000696.*T.^3)/1e6;

s_ch = 'PYSGAH02'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTAH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGAH03'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTAH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGSBLAS'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTABH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index\

s_ch = 'PYSGSBLFS'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTAH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGSBRAS'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTABH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGSBRFS'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTABH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGSPX'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTLH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGSPY'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTLH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index
%%
%CALCGROUP 3%
tc_eqn = @(u, T) u-(-69.6+1.21.*T-0.00302.*T.^2+0.00000544.*T.^3-0.000000179.*T.^4+0.000000000721.*T.^5)/1e6;

s_ch = 'PYSGP1'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTFTC.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGP201'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTFTC.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGP202'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTFTC.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGP3'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTFTC.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGP401'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTABH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

s_ch = 'PYSGP402'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); count = count+1;
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTABH.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index

tc_eqn = @(u, T) u-(-69.6+1.21.*T-0.00302.*T.^2+0.00000544.*T.^3-0.000000179.*T.^4+0.000000000721.*T.^5+(7*(T-75)))/1e6;
s_ch = 'PYSGAMS'; ch_list(count) = string(s_ch);
waitbar(count/(numel(strain_channels)-5),w); close(w);
tare_val = standard_calc.Standard_Calcs.(strcat(s_ch, '_TC')).value(stand_calc_tare_ind);

PY_STRAIN.(group).(s_ch) = strain_fullrate.(strain_channel_group).(s_ch);
[~, py_strain_tare_ind] = min(abs(strain_fullrate.(strain_channel_group).(s_ch).utc_time - utc_target)); %find index where tare occurs in fullrate data

PY_STRAIN.(group).(s_ch).value = tc_eqn(strain_fullrate.(strain_channel_group).(s_ch).value, rtd_fullrate.PYTAMS.value); %thermal compensation applied to fullrate strain data
PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) = PY_STRAIN.(group).(s_ch).value(py_strain_tare_ind:end) - tare_val; %add tare value after index
%%
%Remove empty channels
for ii = 1:numel(strain_channels)
    if(isempty(PY_STRAIN.(group).(strain_channels{ii})))
        PY_STRAIN.(group) = rmfield(PY_STRAIN.(group),strain_channels{ii});
    end
end

% %Uncomment to plot all channels, if desired:
% for ii = 1:numel(ch_list)
%     figure(ii); plot(PY_STRAIN.(group).(ch_list(ii)).utc_time, PY_STRAIN.(group).(ch_list(ii)).value, 'b', standard_calc.Standard_Calcs.(strcat(ch_list(ii), '_TC')).utc_time, standard_calc.Standard_Calcs.(strcat(ch_list(ii), '_TC')).value, 'r');
%     grid on; legend(ch_list(ii),strcat(ch_list(ii),' Standard Calc'));
% end

save_yn = questdlg('Would you like to save the file directly? Data is automatically loaded to workspace as .mat file upon completion.', 'Save?');
if isequal(save_yn, 'Yes')
    w = msgbox('Saving Data... This May Take A While, Please Wait...','modal');
    [outfile, outpath] = uiputfile('*.mat', 'Save data set as...');
    
    out = fullfile(outpath, outfile);

    save(out,'PY_STRAIN', '-v7.3')
    
    try
        close(w); %try catch in case user closes the msgbox before it automatically closes.
    catch
    end
elseif (isequal(save_yn, 'No') || isequal(save_yn, 'Cancel'))
end

output_data = PY_STRAIN;
end

